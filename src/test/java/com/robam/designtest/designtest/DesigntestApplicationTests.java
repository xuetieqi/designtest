package com.robam.designtest.designtest;

import com.robam.designtest.designtest.action.command.Light;
import com.robam.designtest.designtest.action.command.LightOffCommand;
import com.robam.designtest.designtest.action.command.LightOnCommand;
import com.robam.designtest.designtest.action.command.RemoteConrol;
import com.robam.designtest.designtest.action.mediator.*;
import com.robam.designtest.designtest.action.observer.Email;
import com.robam.designtest.designtest.action.observer.Order;
import com.robam.designtest.designtest.action.observer.jdkobserver.RealObserver;
import com.robam.designtest.designtest.action.observer.jdkobserver.RealSubject;
import com.robam.designtest.designtest.action.responsibility.*;
import com.robam.designtest.designtest.action.state.*;
import com.robam.designtest.designtest.action.strategy.AlipayLogin;
import com.robam.designtest.designtest.action.strategy.WechatLogin;
import com.robam.designtest.designtest.action.template.HookAbstractClass;
import com.robam.designtest.designtest.action.template.HookConcreteClass;
import com.robam.designtest.designtest.class1.alarm.Alarm;
import com.robam.designtest.designtest.class1.device.Device;
import com.robam.designtest.designtest.class1.ticket.Ticket;
import com.robam.designtest.designtest.construct.adapter.Adaptee;
import com.robam.designtest.designtest.construct.adapter.Adapter;
import com.robam.designtest.designtest.construct.adapter.Adapter2;
import com.robam.designtest.designtest.construct.bridge.*;
import com.robam.designtest.designtest.construct.decorator.*;
import com.robam.designtest.designtest.construct.proxy.Target;
import com.robam.designtest.designtest.construct.proxy.TargetProxy;
import com.robam.designtest.designtest.construct.proxy.TargetServer;
import com.robam.designtest.designtest.construct.proxy.cglibproxy.CglibTargetProxy;
import com.robam.designtest.designtest.construct.proxy.jdkproxy.JdkTargetProxy;
import com.robam.designtest.designtest.create.factory.factorymethod.Product1Factory;
import com.robam.designtest.designtest.create.factory.factorymethod.Product2Factory;
import com.robam.designtest.designtest.create.factory.product.AbstractProduct;
import com.robam.designtest.designtest.create.factory.simplefactory.ProductFactory;
import lombok.extern.slf4j.Slf4j;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.DependsOn;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
@Slf4j
@DependsOn("SpringUtils")
class DesigntestApplicationTests {
    @Autowired
    ProductFactory productFactory;
    @Autowired
    Product1Factory product1Factory;
    @Autowired
    Product2Factory product2Factory;
    @Autowired
    Adapter adapter;
    @Autowired
    Adapter2 adapter2;

    /**
     * 简单工程制造实例测试
     */
    @Test
    void simpleFactory() {
        AbstractProduct product1 = productFactory.createProduct("1");
        product1.print();
        AbstractProduct product2 = productFactory.createProduct("2");
        product2.print();
    }

    /**
     * 工程方法制造实例测试
     */
    @Test
    void factoryMethod() {
        AbstractProduct product1 = product1Factory.createProduct();
        product1.print();
        AbstractProduct product2 = product2Factory.createProduct();
        product2.print();
    }

    /**
     * 什么时候该用工厂方法模式，而非简单工厂模式?
     * 之所以将某个代码块剥离独立为函数或类，原因是这个代码块的逻辑过于复杂，剥离之后能让代码更加清晰，更加可读、可维护。
     * 但是，如果代码块本身并不复杂，就几行代码而已，我们完全没必要将它拆分成单独的函数或者类。
     * 基于这个设计思想，当对象的创建逻辑比较复杂，不只是简单的new一下，而是要组合其他类对象，做各种初始化操作的时候，推荐使用工厂方法模式，
     * 将复杂的创建逻辑拆分到多个工厂类中，让每个工厂类都不至于过于复杂。而使用简单工厂模式，将所有的创建逻辑都放到一个工厂类中，会导致这个工厂类变得复杂。
     * 除此之外，在某些场景下，如果对象不可复用，那工厂类每次都要返回不同对象。
     * 如果我们使用简单工厂模式实现，就只能选择第一种包含if分支逻辑的实现方式。如果我们还想避免烦人的if-else分支逻辑，推荐使用工厂方法模式。
     */

    /**
     * 测试适配模式 本来要输入字符串的 适配可以输入数字
     */
    @Test
    void adapter() {
        adapter.print(43);
        adapter2.setAdaptee(new Adaptee());
        adapter2.print(43);
    }

    /**
     * 桥接模式
     */
    @Test
    void brige() {
        AbstractMessage abstractMessage = new CommonMessage(new EmailService());
        abstractMessage.send();
        abstractMessage = new UrgencyMessage(new SmsService());
        abstractMessage.send();
    }

    /**
     * 装饰模式
     */
    @Test
    void decorator() {
        Noodle noodle = new SaltDecorator(new BeefNoodle());
        noodle.print();
        noodle = new SugarDecorator(new EggNoodle());
        noodle.print();
    }

    /**
     * 静态代理测试
     */
    @Test
    void staticProxy() {
        Target target = new TargetProxy(new TargetServer());
        target.print();
    }

    /**
     * jdk自带动态代理  只能支持interface接口代理
     * 相比较静态代理  可以代理任意接口不需要重复新增代理类，当然你的代理逻辑新增除外
     */
    @Test
    void jdkProxy() {
        com.robam.designtest.designtest.construct.proxy.jdkproxy.Target target = (com.robam.designtest.designtest.construct.proxy.jdkproxy.Target) JdkTargetProxy.proxy(com.robam.designtest.designtest.construct.proxy.jdkproxy.Target.class, new com.robam.designtest.designtest.construct.proxy.jdkproxy.TargetServer());
        target.print();
    }

    /**
     * cglib代理
     */
    @Test
    void cglibProxy() {
        Target target = (Target) new CglibTargetProxy().getProxy(TargetServer.class);
        target.print();

        com.robam.designtest.designtest.construct.proxy.jdkproxy.Target target2 = (com.robam.designtest.designtest.construct.proxy.jdkproxy.Target) new CglibTargetProxy().getProxy(com.robam.designtest.designtest.construct.proxy.jdkproxy.TargetServer.class);
        target2.print();
    }

    /**
     * 策略模式
     */
    @Test
    void strategy() {
        com.robam.designtest.designtest.action.strategy.Context context = new com.robam.designtest.designtest.action.strategy.Context(new WechatLogin());
        context.doLogin();
        context = new com.robam.designtest.designtest.action.strategy.Context(new AlipayLogin());
        context.doLogin();
    }


    /**
     * 观察者模式
     */
    @Test
    void observer() {
        Order order = new Order();
        order.attach(new Email());
        order.print();
    }

    /**
     * jdk自带观察者模式
     */
    @Test
    void jdkObserver() {
        RealSubject realSubject = new RealSubject();
        RealObserver realObserver = new RealObserver();
        realSubject.addObserver(realObserver);
        realSubject.print();
    }

    /**
     * 责任链模式
     */
    @Test
    void responsibility() {
        Logger logger = new ErrorLogger(new WarmLogger(new InfoLogger(new DebugLogger(null))));
        logger.logMessage(Logger.DEBUG, "被打印的日志信息");
    }

    /**
     * 命令模式
     */
    @Test
    void command() {
        Light lignt = new Light();
        RemoteConrol remoteConrol = new RemoteConrol(new LightOffCommand(lignt));
        remoteConrol.buttonWasPressed();
        remoteConrol = new RemoteConrol(new LightOnCommand(lignt));
        remoteConrol.buttonWasPressed();
    }

    /**
     * 中介者模式
     */
    @Test
    void mediator() {
        //构建中介者
        Mediator mediator = new ConcreteMediator();
        //构建具体的同事类
        Colleague colleague1 = new ConcreteColleague1();
        Colleague colleague2 = new ConcreteColleague2();
        //注册同事
        mediator.register(colleague1);
        mediator.register(colleague2);
        //发送消息
        colleague1.send();
        colleague2.send();
    }

    /**
     * 状态模式
     */
    @Test
    void state() {
        ThreadContext threadContext = new ThreadContext();
        threadContext.getState().handle(threadContext, new ThreadStatusB());
        threadContext.getState().handle(threadContext, new ThreadStatusD());
        threadContext.getState().handle(threadContext, new ThreadStatusC());
        threadContext.getState().handle(threadContext, new ThreadStatusA());
    }


    /**
     * 模板模式
     */
    @Test
    void template() {
        HookAbstractClass tm = new HookConcreteClass();
        tm.TemplateMethod();
    }


    /**
     * 模拟 设备  告警  工单  流转
     */
    @Test
    void class1() throws InterruptedException {
        log.info("//step1 新增设备");
        Device device = Device.builder().id(1L).desc("我是一号设备").status("新增状态").build();
        log.info("device={}", device);
        device.newDevice();
        log.info("do newDevice");
        log.info("device={}", device);


        log.info("//step2 收到告警");
        Alarm alarm = Alarm.builder().id(1L).desc("我是一号告警").status("告警中状态").build();
        alarm.newAlarm(device);
        log.info("do newAlarm");
        log.info("alarm={}", alarm);
        Thread.sleep(1000);
        log.info("device={}", device);

        log.info("//step3 生成并派发工单");
        Ticket ticket = Ticket.builder().id(1L).desc("我是一号工单").status("新增状态").build();
        ticket.newTicket();
        ticket.dealingTicket(alarm, device);
        log.info("do dealingTicket");
        Thread.sleep(1000);
        log.info("device={}", device);
        log.info("alarm={}", alarm);
        log.info("ticket={}", ticket);


        log.info("//step4 工单转派");
        ticket.cancel(alarm, device);
        log.info("do cancelTicket");
        Thread.sleep(1000);
        log.info("device={}", device);
        log.info("alarm={}", alarm);
        log.info("ticket={}", ticket);


        log.info("//step5 工单重新派发");
        ticket.dealingTicket(alarm, device);
        log.info("do dealingTicket");
        Thread.sleep(1000);
        log.info("device={}", device);
        log.info("alarm={}", alarm);
        log.info("ticket={}", ticket);


//        log.info("//step6 工单处理完成");
//        ticket.finishTicket(alarm, device);
//        log.info("do finishTicket");
//        Thread.sleep(1000);
//        log.info("device={}", device);
//        log.info("alarm={}", alarm);
//        log.info("ticket={}", ticket);


        log.info("//step6 工单放弃处理");
        ticket.giveUpTicket(alarm, device);
        log.info("do giveUpTicket");
        Thread.sleep(1000);
        log.info("device={}", device);
        log.info("alarm={}", alarm);
        log.info("ticket={}", ticket);
    }
}
