/**
 * COPYRIGHT Robam Industrial Group Co. Ltd. Limited
 * All right reserved.
 */
package com.robam.designtest.designtest.construct.bridge;

import lombok.extern.slf4j.Slf4j;

/**
 * @Description
 * @Author 薛铁琪
 * @CreateTime 2021/3/25 16:41
 * @Version 1.0
 */
@Slf4j
public class SmsService implements MessageImplementor {
    @Override
    public void send() {
        log.info("准备使用短信发送消息");
    }
}
