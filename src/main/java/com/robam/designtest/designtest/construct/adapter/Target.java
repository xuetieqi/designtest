package com.robam.designtest.designtest.construct.adapter;

/**
 * @author r201069
 */
public interface Target {

    /**
     * 适配方法
     *
     * @param integer
     */
    void print(Integer integer);
}
