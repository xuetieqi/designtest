/**
 * COPYRIGHT Robam Industrial Group Co. Ltd. Limited
 * All right reserved.
 */
package com.robam.designtest.designtest.construct.proxy.cglibproxy;

import lombok.extern.slf4j.Slf4j;
import net.sf.cglib.proxy.Enhancer;
import net.sf.cglib.proxy.MethodInterceptor;
import net.sf.cglib.proxy.MethodProxy;

import java.lang.reflect.Method;

/**
 * @Description
 * @Author 薛铁琪
 * @CreateTime 2021/3/26 9:18
 * @Version 1.0
 */
@Slf4j
public class CglibTargetProxy implements MethodInterceptor {

    public Enhancer enhancer = new Enhancer();


    /**
     * 根据class对象创建该对象的代理对象
     * 1、设置父类；2、设置回调
     * 本质：动态创建了一个class对象的子类
     */
    public Object getProxy(Class target) {
        enhancer.setSuperclass(target);
        enhancer.setCallback(this);
        return enhancer.create();
    }


    @Override
    public Object intercept(Object o, Method method, Object[] objects, MethodProxy methodProxy) throws Throwable {
        log.info("before invoke");
        log.info(method.toString());
        Object object = methodProxy.invokeSuper(o, objects);
        log.info("after invoke");
        return object;
    }
}
