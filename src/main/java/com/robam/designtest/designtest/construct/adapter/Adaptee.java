/**
 * COPYRIGHT Robam Industrial Group Co. Ltd. Limited
 * All right reserved.
 */
package com.robam.designtest.designtest.construct.adapter;

import lombok.extern.slf4j.Slf4j;

/**
 * @Description
 * @Author 薛铁琪
 * @CreateTime 2021/3/25 16:02
 * @Version 1.0
 */
@Slf4j
public class Adaptee {
    public void print(String str) {
        log.info("输入字符串");
    }
}
