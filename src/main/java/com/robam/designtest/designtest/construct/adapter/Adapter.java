/**
 * COPYRIGHT Robam Industrial Group Co. Ltd. Limited
 * All right reserved.
 */
package com.robam.designtest.designtest.construct.adapter;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @Description
 * @Author 薛铁琪
 * @CreateTime 2021/3/25 16:02
 * @Version 1.0
 */
@Slf4j
@Component
public class Adapter extends Adaptee implements Target {
    @Override
    public void print(Integer integer) {
        log.info("输入数字");
        log.info("数字转换字符串");
        super.print(integer.toString());
    }
}
