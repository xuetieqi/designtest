/**
 * COPYRIGHT Robam Industrial Group Co. Ltd. Limited
 * All right reserved.
 */
package com.robam.designtest.designtest.construct.adapter;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @Description
 * @Author 薛铁琪
 * @CreateTime 2021/3/25 16:02
 * @Version 1.0
 */
@Slf4j
@Component
@Data
public class Adapter2 implements Target {
    private Adaptee adaptee;

    @Override
    public void print(Integer integer) {
        log.info("输入数字");
        log.info("数字转换字符串");
        adaptee.print(integer.toString());
    }
}
