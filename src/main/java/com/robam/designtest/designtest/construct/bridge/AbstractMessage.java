/**
 * COPYRIGHT Robam Industrial Group Co. Ltd. Limited
 * All right reserved.
 */
package com.robam.designtest.designtest.construct.bridge;

/**
 * @Description
 * @Author 薛铁琪
 * @CreateTime 2021/3/25 16:32
 * @Version 1.0
 */
public abstract class AbstractMessage {

    public MessageImplementor messageImplementor;

    public AbstractMessage(MessageImplementor messageImplementor) {
        this.messageImplementor = messageImplementor;
    }

    public abstract void send();
}
