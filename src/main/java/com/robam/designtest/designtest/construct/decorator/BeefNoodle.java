/**
 * COPYRIGHT Robam Industrial Group Co. Ltd. Limited
 * All right reserved.
 */
package com.robam.designtest.designtest.construct.decorator;

import lombok.extern.slf4j.Slf4j;

/**
 * @Description
 * @Author 薛铁琪
 * @CreateTime 2021/3/25 17:08
 * @Version 1.0
 */
@Slf4j
public class BeefNoodle extends Noodle {
    @Override
    public void print() {
        log.info("我是牛肉面");
    }
}
