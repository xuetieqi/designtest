/**
 * COPYRIGHT Robam Industrial Group Co. Ltd. Limited
 * All right reserved.
 */
package com.robam.designtest.designtest.create.factory.factorymethod;

import com.robam.designtest.designtest.create.factory.product.AbstractProduct;
import com.robam.designtest.designtest.create.factory.product.Product2;
import org.springframework.stereotype.Component;

/**
 * @Description
 * @Author 薛铁琪
 * @CreateTime 2021/3/25 11:30
 * @Version 1.0
 */
@Component
public class Product2Factory extends AbstractProductFactory {
    @Override
    public AbstractProduct createProduct() {
        return new Product2();
    }
}
