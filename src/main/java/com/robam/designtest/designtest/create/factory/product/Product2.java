/**
 * COPYRIGHT Robam Industrial Group Co. Ltd. Limited
 * All right reserved.
 */
package com.robam.designtest.designtest.create.factory.product;

import lombok.extern.slf4j.Slf4j;

/**
 * @Description
 * @Author 薛铁琪 继承产品抽象类的子类 卡车
 * @CreateTime 2021/3/25 11:04
 * @Version 1.0
 */
@Slf4j
public class Product2 extends AbstractProduct {

    @Override
    public void print() {
        log.info("this is product2");
    }
}
