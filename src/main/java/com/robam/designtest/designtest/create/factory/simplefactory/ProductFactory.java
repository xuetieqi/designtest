/**
 * COPYRIGHT Robam Industrial Group Co. Ltd. Limited
 * All right reserved.
 */
package com.robam.designtest.designtest.create.factory.simplefactory;

import com.robam.designtest.designtest.create.factory.product.AbstractProduct;
import com.robam.designtest.designtest.create.factory.product.Product1;
import com.robam.designtest.designtest.create.factory.product.Product2;
import org.springframework.stereotype.Component;

/**
 * @Description
 * @Author 薛铁琪 简单工程类 用于生产抽象产品的子类的实例 卡车1 卡车2 跑车1 跑车2
 * @CreateTime 2021/3/25 11:12
 * @Version 1.0
 */
@Component
public class ProductFactory {

    public AbstractProduct createProduct(String type) {
        if ("1".equals(type)) {
            return new Product1();
        } else {
            return new Product2();
        }
    }
}
