/**
 * COPYRIGHT Robam Industrial Group Co. Ltd. Limited
 * All right reserved.
 */
package com.robam.designtest.designtest.create.factory.factorymethod;

import com.robam.designtest.designtest.create.factory.product.AbstractProduct;

/**
 * @Description
 * @Author 薛铁琪
 * @CreateTime 2021/3/25 11:28
 * @Version 1.0
 */
public abstract class AbstractProductFactory {
    /**
     * 生产产品
     *
     * @return
     */
    public abstract AbstractProduct createProduct();
}
