package com.robam.designtest.designtest.class1.alarm.listen;

import com.robam.designtest.designtest.class1.Request;
import com.robam.designtest.designtest.class1.Response;
import com.robam.designtest.designtest.class1.alarm.filter.*;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

/**
 * @Description
 * @Author 薛铁琪
 * @CreateTime 2021/5/14 14:11
 * @Version 1.0
 */
@Component
public class AlarmNotifyEventListener {


    @EventListener
    @Async
    public void listener(AlarmNotifyEvent event) {
        Request request = (Request) event.getData();
        DealingFilter dealingFilter = new DealingFilter();
        CancelFilter cancelFilter = new CancelFilter();
        FinishFilter finishFilter = new FinishFilter();
        GiveUpFilter giveUpFilter = new GiveUpFilter();
        AlarmTransferFilterChain alarmTransferFilterChain = new AlarmTransferFilterChain();
        alarmTransferFilterChain.addFilter(dealingFilter).addFilter(cancelFilter).addFilter(finishFilter).addFilter(giveUpFilter);
        alarmTransferFilterChain.doFilter(request, new Response(), alarmTransferFilterChain);
    }
}
