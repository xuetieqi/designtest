/**
 * COPYRIGHT HangZhou 99Cloud Technology Company Limited
 * All right reserved.
 */
package com.robam.designtest.designtest.class1.alarm.listen;

import com.robam.designtest.designtest.SpringUtils;
import org.springframework.stereotype.Component;

/**
 * @Description
 * @Author 薛铁琪
 * @CreateTime 2021/5/31 15:48
 * @Version 1.0
 */
@Component
public class AlarmNotifyPublisher<T> {

    public boolean publishNotify(T request) {
        AlarmNotifyEvent alarmNotifyEvent = new AlarmNotifyEvent(this, request);
        SpringUtils.getApplicationContext().publishEvent(alarmNotifyEvent);
        return true;
    }
}
