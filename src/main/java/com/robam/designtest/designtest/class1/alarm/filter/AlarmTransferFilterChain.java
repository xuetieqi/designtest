/**
 * COPYRIGHT HangZhou 99Cloud Technology Company Limited
 * All right reserved.
 */
package com.robam.designtest.designtest.class1.alarm.filter;

import com.robam.designtest.designtest.class1.Request;
import com.robam.designtest.designtest.class1.Response;

import java.util.ArrayList;
import java.util.List;

/**
 * @Description
 * @Author 薛铁琪
 * @CreateTime 2021/10/13 14:30
 * @Version 1.0
 */
public class AlarmTransferFilterChain implements AlarmTransferFilter {

    private List<AlarmTransferFilter> filters = new ArrayList<AlarmTransferFilter>();
    int index = 0;

    public AlarmTransferFilterChain addFilter(AlarmTransferFilter filter) {
        this.filters.add(filter);
        return this;
    }


    @Override
    public void doFilter(Request req, Response res, AlarmTransferFilterChain chain) {
        if (index == filters.size()) {
            return;
        }
        AlarmTransferFilter filter = filters.get(index);
        index++;
        filter.doFilter(req, res, chain);
    }
}