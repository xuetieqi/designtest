/**
 * COPYRIGHT HangZhou 99Cloud Technology Company Limited
 * All right reserved.
 */
package com.robam.designtest.designtest.class1.device.listen;

import com.robam.designtest.designtest.SpringUtils;
import org.springframework.stereotype.Component;

/**
 * @Description
 * @Author 薛铁琪
 * @CreateTime 2021/5/31 15:48
 * @Version 1.0
 */
@Component
public class DeviceNotifyPublisher<T> {

    public boolean publishNotify(T message) {
        DeviceNotifyEvent deviceNotifyEvent = new DeviceNotifyEvent(this, message);
        SpringUtils.getApplicationContext().publishEvent(deviceNotifyEvent);
        return true;
    }
}
