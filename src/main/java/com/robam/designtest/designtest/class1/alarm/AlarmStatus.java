/**
 * COPYRIGHT HangZhou 99Cloud Technology Company Limited
 * All right reserved.
 */
package com.robam.designtest.designtest.class1.alarm;

import com.robam.designtest.designtest.class1.device.Device;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

/**
 * @Description 告警状态子类
 * @Author 薛铁琪
 * @CreateTime 2021/10/12 14:52
 * @Version 1.0
 */
@Data
@Slf4j
public abstract class AlarmStatus {

    private Alarm alarm;

    public AlarmStatus(Alarm alarm) {
        this.alarm = alarm;
    }


    /**
     * 告警处理中
     *
     * @return
     */
    public AlarmStatus dealAlarm(Device device) {
        log.info("you can't do this");
        return this;
    }

    /**
     * 告警处理完成
     *
     * @return
     */
    public AlarmStatus finishAlarm(Device device) {
        log.info("you can't do this");
        return this;
    }


    /**
     * 告警无法处理
     *
     * @return
     */
    public AlarmStatus giveUpAlarm(Device device) {
        log.info("you can't do this");
        return this;
    }

    /**
     * 工单转单 告警恢复开始状态
     *
     * @return
     */
    public AlarmStatus cancelAlarm(Device device) {
        log.info("you can't do this");
        return this;
    }
}
