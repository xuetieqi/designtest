/**
 * COPYRIGHT HangZhou 99Cloud Technology Company Limited
 * All right reserved.
 */
package com.robam.designtest.designtest.class1.ticket;

import com.robam.designtest.designtest.class1.alarm.Alarm;
import com.robam.designtest.designtest.class1.device.Device;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

/**
 * @Description
 * @Author 薛铁琪
 * @CreateTime 2021/10/12 14:52
 * @Version 1.0
 */
@Data
@Slf4j
public abstract class TicketStatus {

    private Ticket ticket;

    public TicketStatus(Ticket ticket) {
        this.ticket = ticket;
    }

    /**
     * 转工单
     *
     * @return
     */
    public TicketStatus cancel(Alarm alarm, Device device) {
        log.info("you can't do this");
        return this;
    }

    /**
     * 处理工单
     *
     * @return
     */
    public TicketStatus dealingTicket(Alarm alarm, Device device) {
        log.info("you can't do this");
        return this;
    }

    /**
     * 完成工单
     *
     * @return
     */
    public TicketStatus finishTicket(Alarm alarm, Device device) {
        log.info("you can't do this");
        return this;
    }


    /**
     * 工单无法处理
     *
     * @return
     */
    public TicketStatus giveUpTicket(Alarm alarm, Device device) {
        log.info("you can't do this");
        return this;
    }
}
