/**
 * COPYRIGHT HangZhou 99Cloud Technology Company Limited
 * All right reserved.
 */
package com.robam.designtest.designtest.class1.device;

/**
 * @Description
 * @Author 薛铁琪
 * @CreateTime 2021/10/12 16:22
 * @Version 1.0
 */
public class ScrapDeviceStatus extends DeviceStatus {

    public ScrapDeviceStatus(Device device) {
        super(device);
    }

}
