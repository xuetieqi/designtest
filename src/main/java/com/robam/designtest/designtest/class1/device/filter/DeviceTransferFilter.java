package com.robam.designtest.designtest.class1.device.filter;

import com.robam.designtest.designtest.class1.Request;
import com.robam.designtest.designtest.class1.Response;

public interface DeviceTransferFilter {


    public void doFilter(Request req, Response response, DeviceTransferFilterChain chain);
}