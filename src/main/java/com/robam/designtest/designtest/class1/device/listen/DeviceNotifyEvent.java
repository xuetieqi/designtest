/**
 * COPYRIGHT HangZhou 99Cloud Technology Company Limited
 * All right reserved.
 */
package com.robam.designtest.designtest.class1.device.listen;

import lombok.Getter;
import org.springframework.context.ApplicationEvent;

/**
 * @Description 所有消息通知事件 短信 微信消息通知 邮件通知等
 * @Author 薛铁琪
 * @CreateTime 2021/5/31 14:44
 * @Version 1.0
 */
@Getter
public class DeviceNotifyEvent<T> extends ApplicationEvent {
    private static final long serialVersionUID = -4016696633642724859L;

    private T data;

    public DeviceNotifyEvent(Object source, T data) {
        super(source);
        this.data = data;
    }
}
