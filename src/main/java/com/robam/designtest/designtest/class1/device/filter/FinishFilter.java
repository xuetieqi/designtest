package com.robam.designtest.designtest.class1.device.filter;

import com.robam.designtest.designtest.class1.Request;
import com.robam.designtest.designtest.class1.Response;
import com.robam.designtest.designtest.class1.device.Device;

public class FinishFilter implements DeviceTransferFilter {

    @Override
    public void doFilter(Request req, Response response, DeviceTransferFilterChain chain) {
        if ("finish".equals(req.getRequest())) {
            //todo 拿到对应的device do cancel;
            Device device = req.getDevice();
            device.normalDevice();
        }
        chain.doFilter(req, response, chain);
    }
}