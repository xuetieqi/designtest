/**
 * COPYRIGHT HangZhou 99Cloud Technology Company Limited
 * All right reserved.
 */
package com.robam.designtest.designtest.class1.alarm;

import com.robam.designtest.designtest.class1.Request;
import com.robam.designtest.designtest.class1.device.Device;
import com.robam.designtest.designtest.class1.device.listen.DeviceNotifyPublisher;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Description 告警类
 * @Author 薛铁琪
 * @CreateTime 2021/10/12 13:50
 * @Version 1.0
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Alarm {

    private Long id;
    private String desc;
    private String status;


    private AlarmStatus alarmStatus;

    /**
     * 数据库拿到对象后 记得初始化alarmStatus
     */
    private void initAlarmStatus() {
        // todo 根据数据库查询出来的status确定当前的alarmStatus
        this.alarmStatus = new StartAlarmStatus(this);
    }

    /******************************************************************************************************************************状态设计模式之 主对象下的流转列表**/


    /**
     * 当告警第一次从万安同步时调用
     */
    public void newAlarm(Device device) {
        // todo 写入数据库
        // todo 设备状态 -->异常
        DeviceNotifyPublisher deviceNotifyPublisher = new DeviceNotifyPublisher();
        deviceNotifyPublisher.publishNotify(Request.builder().id(1L).request("new").alarm(this).device(device).build());
        this.alarmStatus = new StartAlarmStatus(this);
    }

    /**
     * 从开始状态进入处理中时调用
     */
    public void dealingAlarm(Device device) {
        this.alarmStatus = this.alarmStatus.dealAlarm(device);
    }


    /**
     * 从处理状态 处理完成时调用
     */
    public void finishAlarm(Device device) {
        this.alarmStatus = this.alarmStatus.finishAlarm(device);
    }


    /**
     * 从处理状态放弃处理时调用
     */
    public void giveUpAlarm(Device device) {
        this.alarmStatus = this.alarmStatus.giveUpAlarm(device);
    }


    /**
     * 工单转单 从处理状态到告警状态时调用
     */
    public void cancel(Device device) {
        this.alarmStatus = this.alarmStatus.cancelAlarm(device);
    }

    @Override
    public String toString() {
        return "Alarm{" +
                ", id=" + id +
                ", desc='" + desc + '\'' +
                ", status='" + status + '\'' +
                '}';
    }
}
