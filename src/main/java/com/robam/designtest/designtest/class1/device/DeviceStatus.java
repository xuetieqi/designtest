/**
 * COPYRIGHT HangZhou 99Cloud Technology Company Limited
 * All right reserved.
 */
package com.robam.designtest.designtest.class1.device;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

/**
 * @Description
 * @Author 薛铁琪
 * @CreateTime 2021/10/12 14:52
 * @Version 1.0
 */
@Data
@Slf4j
public abstract class DeviceStatus {

    private Device device;

    public DeviceStatus(Device device) {
        this.device = device;
    }

    /**
     * 设备正常
     *
     * @return
     */
    public DeviceStatus normalDevice() {
        log.info("you can't do this");
        return this;
    }

    /**
     * 设备异常
     *
     * @return
     */
    public DeviceStatus abnormalDevice() {
        log.info("you can't do this");
        return this;
    }

    /**
     * 设备异常处理中
     *
     * @return
     */
    public DeviceStatus dealDevice() {
        log.info("you can't do this");
        return this;
    }


    /**
     * 设备报废
     *
     * @return
     */
    public DeviceStatus scrapDevice() {
        log.info("you can't do this");
        return this;
    }


    /**
     * 设备报废
     *
     * @return
     */
    public DeviceStatus cancelDevice() {
        log.info("you can't do this");
        return this;
    }
}
