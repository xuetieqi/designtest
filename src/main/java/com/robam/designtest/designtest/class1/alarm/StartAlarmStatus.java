/**
 * COPYRIGHT HangZhou 99Cloud Technology Company Limited
 * All right reserved.
 */
package com.robam.designtest.designtest.class1.alarm;

import com.robam.designtest.designtest.class1.Request;
import com.robam.designtest.designtest.class1.device.Device;
import com.robam.designtest.designtest.class1.device.listen.DeviceNotifyPublisher;

/**
 * @Description 开始状态能做的动作 以及返回的下一个状态
 * @Author 薛铁琪
 * @CreateTime 2021/10/12 15:11
 * @Version 1.0
 */
public class StartAlarmStatus extends AlarmStatus {


    public StartAlarmStatus(Alarm alarm) {
        super(alarm);
    }

    @Override
    public AlarmStatus dealAlarm(Device device) {

        // todo 更新警告状态 -->处理中
        Alarm alarm = getAlarm();
        alarm.setStatus("处理中");
        // todo 设备状态 --> 处理中
        DeviceNotifyPublisher deviceNotifyPublisher = new DeviceNotifyPublisher();
        deviceNotifyPublisher.publishNotify(Request.builder().id(1L).request("dealing").device(device).build());
        return new DealingAlarmStatus(getAlarm());
    }
}
