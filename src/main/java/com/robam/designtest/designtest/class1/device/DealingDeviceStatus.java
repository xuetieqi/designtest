/**
 * COPYRIGHT HangZhou 99Cloud Technology Company Limited
 * All right reserved.
 */
package com.robam.designtest.designtest.class1.device;

/**
 * @Description
 * @Author 薛铁琪
 * @CreateTime 2021/10/12 16:22
 * @Version 1.0
 */
public class DealingDeviceStatus extends DeviceStatus {

    public DealingDeviceStatus(Device device) {
        super(device);
    }

    @Override
    public DeviceStatus normalDevice() {
        // todo 设备状态 --> 正常
        Device device = getDevice();
        device.setStatus("状态恢复正常状态");
        return new NormalDeviceStatus(getDevice());
    }

    @Override
    public DeviceStatus scrapDevice() {
        // todo 设备状态 --> 报废
        Device device = getDevice();
        device.setStatus("状态变成报废状态");
        return new ScrapDeviceStatus(getDevice());
    }

    @Override
    public DeviceStatus cancelDevice() {
        // todo 设备状态 --> 异常
        Device device = getDevice();
        device.setStatus("状态恢复异常状态");
        return new AbnormalDeviceStatus(getDevice());
    }
}
