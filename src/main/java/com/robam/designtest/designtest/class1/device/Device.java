/**
 * COPYRIGHT HangZhou 99Cloud Technology Company Limited
 * All right reserved.
 */
package com.robam.designtest.designtest.class1.device;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Description
 * @Author 薛铁琪
 * @CreateTime 2021/10/12 13:50
 * @Version 1.0
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Device {

    private Long id;
    private String desc;
    private String status;

    private DeviceStatus deviceStatus;

    /**
     * 数据库拿到对象后 记得初始化deviceStatus
     */
    public void initDeviceStatus() {
        // todo 根据数据库查询出来的status确定当前的deviceStatus
        this.deviceStatus = new NormalDeviceStatus(this);
    }


    /******************************************************************************************************************************状态设计模式之 主对象下的流转列表**/


    /**
     * 当设备第一次从万安同步时调用
     */
    public void newDevice() {
        // todo 写入数据库 根据万安查询出来的status确定当前的deviceStatus
        this.deviceStatus = new NormalDeviceStatus(this);
    }


    /**
     * 从处理状态完成处理恢复正常时调用
     */
    public void normalDevice() {
        this.deviceStatus = this.deviceStatus.normalDevice();
    }

    /**
     * 从正常状态收到告警变成异常时调用
     */
    public void abnormalDevice() {
        this.deviceStatus = this.deviceStatus.abnormalDevice();
    }

    /**
     * 工单派发 进入处理状态时调用
     */
    public void dealingDevice() {
        this.deviceStatus = this.deviceStatus.dealDevice();
    }

    /**
     * 工单转派 恢复异常状态
     */
    public void cancel() {
        this.deviceStatus = this.deviceStatus.cancelDevice();
    }


    /**
     * 从处理状态放弃处理变成报废时调用
     */
    public void scrapDevice() {
        this.deviceStatus = this.deviceStatus.scrapDevice();
    }

    @Override
    public String toString() {
        return "Device{" +
                "id=" + id +
                ", desc='" + desc + '\'' +
                ", status='" + status +
                '}';
    }
}
