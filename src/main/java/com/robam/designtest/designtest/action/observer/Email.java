/**
 * COPYRIGHT Robam Industrial Group Co. Ltd. Limited
 * All right reserved.
 */
package com.robam.designtest.designtest.action.observer;

import lombok.extern.slf4j.Slf4j;

/**
 * @Description
 * @Author 薛铁琪
 * @CreateTime 2021/3/26 11:26
 * @Version 1.0
 */
@Slf4j
public class Email implements Observer {
    @Override
    public void update() {
        log.info("接收到通知，处理了些事情");
    }
}
