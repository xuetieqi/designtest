/**
 * COPYRIGHT Robam Industrial Group Co. Ltd. Limited
 * All right reserved.
 */
package com.robam.designtest.designtest.action.strategy;

/**
 * @Description
 * @Author 薛铁琪
 * @CreateTime 2021/3/26 10:15
 * @Version 1.0
 */
public class Context {
    public Login login;

    public Context(Login login) {
        this.login = login;
    }

    public void doLogin() {
        login.print();
    }
}
