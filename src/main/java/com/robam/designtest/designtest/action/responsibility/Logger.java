/**
 * COPYRIGHT Robam Industrial Group Co. Ltd. Limited
 * All right reserved.
 */
package com.robam.designtest.designtest.action.responsibility;

/**
 * @Description
 * @Author 薛铁琪
 * @CreateTime 2021/3/26 14:59
 * @Version 1.0
 */
public abstract class Logger {
    public static int DEBUG = 1;
    public static int INFO = 2;
    public static int WARM = 3;
    public static int ERROR = 4;

    protected int level;

    public Logger nextLogger;

    public Logger(int level, Logger nextLogger) {
        this.level = level;
        this.nextLogger = nextLogger;
    }

    public void logMessage(int level, String message) {
        if (this.level == level) {
            write(message);
        }
        if (nextLogger != null) {
            nextLogger.logMessage(level, message);
        }
    }

    abstract public void write(String message);
}
