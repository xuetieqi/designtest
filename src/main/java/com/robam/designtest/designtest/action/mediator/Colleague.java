/**
 * COPYRIGHT Robam Industrial Group Co. Ltd. Limited
 * All right reserved.
 */
package com.robam.designtest.designtest.action.mediator;

/**
 * @Description同事抽象类
 * @Author 薛铁琪
 * @CreateTime 2021/3/27 10:49
 * @Version 1.0
 */
public abstract class Colleague {
    //中介者抽象类
    protected Mediator mediator;

    //接受消息
    public abstract void receive();

    //发送消息
    public abstract void send();

    public void setMediator(Mediator mediator) {
        this.mediator = mediator;
    }
}
