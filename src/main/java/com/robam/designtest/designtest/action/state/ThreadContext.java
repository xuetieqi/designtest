/**
 * COPYRIGHT Robam Industrial Group Co. Ltd. Limited
 * All right reserved.
 */
package com.robam.designtest.designtest.action.state;

import lombok.Data;

/**
 * @Description 线程控制器 状态模式 ：状态决定行为、状态之间可以变动
 * @Author 薛铁琪
 * @CreateTime 2021/3/27 12:49
 * @Version 1.0
 */
@Data
public class ThreadContext {

    private ThreadState state = new ThreadStatusA();
}
