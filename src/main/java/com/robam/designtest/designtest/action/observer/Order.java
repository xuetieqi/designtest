/**
 * COPYRIGHT Robam Industrial Group Co. Ltd. Limited
 * All right reserved.
 */
package com.robam.designtest.designtest.action.observer;

import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;

/**
 * @Description
 * @Author 薛铁琪
 * @CreateTime 2021/3/26 11:25
 * @Version 1.0
 */
@Slf4j
public class Order implements Subject {
    private List<Observer> observerList = new ArrayList<>();

    @Override
    public void attach(Observer observer) {
        observerList.add(observer);

    }

    @Override
    public void detach(Observer observer) {
        observerList.remove(observer);

    }

    @Override
    public void notifyChanged() {
        for (Observer observer : observerList) {
            observer.update();
        }
    }

    public void print() {
        log.info("do something");
        notifyChanged();
    }
}
