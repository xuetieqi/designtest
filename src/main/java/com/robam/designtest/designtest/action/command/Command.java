/**
 * COPYRIGHT Robam Industrial Group Co. Ltd. Limited
 * All right reserved.
 */
package com.robam.designtest.designtest.action.command;

/**
 * @Description
 * @Author 薛铁琪
 * @CreateTime 2021/3/27 10:19
 * @Version 1.0
 */
public interface Command {

    /**
     * 命令执行
     */
    public void execute();
}
