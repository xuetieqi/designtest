/**
 * COPYRIGHT Robam Industrial Group Co. Ltd. Limited
 * All right reserved.
 */
package com.robam.designtest.designtest.action.state;

/**
 * @Description
 * @Author 薛铁琪
 * @CreateTime 2021/3/27 12:49
 * @Version 1.0
 */
public abstract class ThreadState {

    public String state;

    /**
     * 处理线程状态变化
     *
     * @param threadContext
     * @param next
     */
    public abstract void handle(ThreadContext threadContext, ThreadState next);

}
