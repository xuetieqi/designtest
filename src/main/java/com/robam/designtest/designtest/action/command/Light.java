/**
 * COPYRIGHT Robam Industrial Group Co. Ltd. Limited
 * All right reserved.
 */
package com.robam.designtest.designtest.action.command;

import lombok.extern.slf4j.Slf4j;

/**
 * @Description 为了简单这里的灯是类，生产时有可能是一个服务的单例被调用
 * @Author 薛铁琪
 * @CreateTime 2021/3/27 10:19
 * @Version 1.0
 */
@Slf4j
public class Light {
    public void on() {
        log.info("light is on");
    }

    public void off() {
        log.info("light is off");
    }
}
