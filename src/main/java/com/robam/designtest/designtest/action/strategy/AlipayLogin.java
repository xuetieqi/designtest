/**
 * COPYRIGHT Robam Industrial Group Co. Ltd. Limited
 * All right reserved.
 */
package com.robam.designtest.designtest.action.strategy;

import lombok.extern.slf4j.Slf4j;

/**
 * @Description
 * @Author 薛铁琪
 * @CreateTime 2021/3/26 10:14
 * @Version 1.0
 */
@Slf4j
public class AlipayLogin implements Login {
    @Override
    public void print() {
        log.info("实现支付宝登陆");
    }
}
